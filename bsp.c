// Formatting my way (sorry not sorry)
void bsp(Monitor *mon)
{
	unsigned int i, n, nx, ny, nw, nh;
	unsigned int xoff, yoff, ww, wh;
	unsigned int gap, ogap;
	Client *c;

	// For all clients
	for(n = 0, c = nexttiled(mon->clients); c; c = nexttiled(c->next), n++);

	if(n == 0)
		return;

	gap = mon->gappx;
	ogap = mon->ogap;

	ww = mon->ww - (2 * ogap);
	wh = mon->wh - (2 * ogap);

	xoff = ogap + mon->wx;
	yoff = ogap + mon->wy;

	// Values of the last window
	nx = xoff;
	ny = yoff;
	nw = ww;
	nh = wh;

	//i is window counter
	for(i = 0, c = nexttiled(mon->clients); c; c = nexttiled(c->next))
	{
		// Unecessary brackets give extra clarity (not that it helps much)
		if(( (i % 2) && (nh / 2) > (2 * c->bw))
				|| (!(i % 2) && (nw / 2) > (2 * c->bw)))
		{
			// Position Clients
			if(i > 1) // Every other client (Except 1 and 2)
			{
				if(i % 2 == 1)
				{
					if(i != n - 1)
						nh = (nh/2) - (gap / 2);
					nx = nx + nw + gap;
				}else if(i % 2 == 0)
				{
					if(i != n - 1)
						nw = (nw/2) - (gap / 2);
					ny = ny + nh + gap;
				}
			}

			// If this is the first client
			if(i == 0)
			{
				// And there are more clients to come
				if(n != 1)
					nw = ww * mon->mfact - (gap / 2);
				ny = yoff;
			}

			// Second client
			if(i == 1)
			{
				nx = nx + nw + gap;
				nw = (ww - nw) - gap;
				if(i != (n - 1)) // We are not the last window
					nh = (nh / 2) - (gap / 2);
			}
			i++;
		}
		resize(c, nx, ny, nw - 2 * c->bw, nh - 2 * c->bw, False);
	}
}
